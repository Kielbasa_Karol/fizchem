import java.util.List;

public class Helper {
    public static <T> T getLastItem(List<T> list) {
        if (list == null || list.isEmpty())
            return null;
        return list.get(list.size() - 1);
    }
}

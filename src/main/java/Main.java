import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Kiełbasa Karol on 21.10.2018.
 */
public class Main {

    public static void main(String[] args) throws IOException {
//        File file = new File("Specific_Heat.txt");
//        Scanner in = new Scanner(file);
//        BufferedReader reader = new BufferedReader(new FileReader("Specific_Heat.txt"));
//        String line = reader.readLine();
//        while (line != null) {
//            System.out.println(line);
//            String[] parts = line.split(" ");
//            temperature.add(Double.parseDouble(parts[0]) + 273.15); // from C to K
//            specificHeat.add(Double.parseDouble(parts[1]));
//            line = reader.readLine();
//        }
//        System.out.println(temperature);
//        System.out.println(specificHeat);
//        f = interp.interpolate(temperature.stream().mapToDouble(Double::doubleValue).toArray(),
//                specificHeat.stream().mapToDouble(Double::doubleValue).toArray());
//
////        System.out.println(f.value(350));
//        enthalpy();
        Logic logic = new Logic();
//        logic.calculateEntalphyStart(500.0,0.0,150.0);

        JFrame frame = new JFrame("MainForm");
        frame.setContentPane(new Window(logic).panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);



//        SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//
//                JFrame frame = new JFrame("Charts");
//
//                frame.setSize(600, 400);
//                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//                frame.setVisible(true);
//
//                XYDataset ds = createDataset();
//                JFreeChart chart = ChartFactory.createXYLineChart("Test Chart",
//                        "x", "y", ds, PlotOrientation.VERTICAL, true, true,
//                        false);
//
//                ChartPanel cp = new ChartPanel(chart);
//
//                frame.getContentPane().add(cp);
//            }
//        });
    }




}

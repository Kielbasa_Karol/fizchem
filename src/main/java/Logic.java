import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.apache.commons.math3.analysis.integration.TrapezoidIntegrator;


import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kiełbasa Karol on 21.10.2018.
 */
public class Logic {
    private ArrayList<Double> temperature = new ArrayList<>();
    private ArrayList<Double> specificHeat = new ArrayList<>();

    private List<Double> enthalpyInterpolation = new ArrayList<>();
    private List<Double> specificHeatInterpolation = new ArrayList<>();
    private List<Double> temperatureForInterpolation = new ArrayList<>();

    private static LinearInterpolator interp = new LinearInterpolator();
    private static PolynomialSplineFunction f;

    public Logic() throws IOException {
        loadDataFromTxt();
    }

    private Double countEntalphy(Double lastEntalpy, Double temp, Double lastTemp) {
        Double heat = f.value(temp);
        if (lastEntalpy == null)
            return temp * heat;
        else {
            Double lastHeat = f.value(temp);
            return lastEntalpy + ((heat + lastHeat) / 2 * (temp - lastTemp));
        }
    }

    public void calculateEntalphyEnd(Double t1, Double t2, Double entalpyFromUser){
        calculateEntalphyAddAllEntalphyFromAfterTemp(t2, entalpyFromUser);
    }

    public void calculateEntalphyStart(Double t1, Double t2, Double entalpyFromUser) {
        calculateEntalphyAddAllEntalphyFromAfterTemp(t1,entalpyFromUser);
    }

    private void calculateEntalphyAddAllEntalphyFromAfterTemp(Double activeTemperature, Double entalpyFromUser){
        clearDatasetList();
        Double startTemp = temperature.get(0);
        Double endTemp = Helper.getLastItem(temperature);
        Double currentTemp = startTemp;
        int step = 1;

        boolean isUsed = false;

        while (currentTemp <= endTemp) {
            Double lastEntalphy = Helper.getLastItem(enthalpyInterpolation);
            Double lastTemp = currentTemp - step;
            Double currentHeat = f.value(currentTemp);
            Double countedEntalphy = countEntalphy(lastEntalphy, currentTemp, lastTemp);

            if (currentTemp >= activeTemperature && !isUsed) {
                addItemToDataSet(currentTemp, currentHeat,countedEntalphy + entalpyFromUser);
                isUsed = true;
            } else {
                addItemToDataSet(currentTemp, currentHeat, countedEntalphy);
            }
            currentTemp += step;
        }
    }

    private void addItemToDataSet(Double temp, Double specificHeat, Double enthalpy) {
        temperatureForInterpolation.add(temp);
        specificHeatInterpolation.add(specificHeat);
        enthalpyInterpolation.add(enthalpy);
    }


    public void calculateEntalphyGamma(Double t1, Double t2, Double entalpyFromUser, Double shape, Double scale) {
        clearDatasetList();
        Double startTemp = temperature.get(0);
        Double endTemp = Helper.getLastItem(temperature);
        Double currentTemp = startTemp;
        int step = 1;

        while (currentTemp <= endTemp) {
            if (currentTemp > t1 && currentTemp < t2) {
                TrapezoidIntegrator trapezoid = new TrapezoidIntegrator();
                Double interval = (t2 - t1) / 20;
                for (int j = 0; j < 20; j++) {
                    Double valueFromDistribution = entalpyFromUser * trapezoid.integrate(100000, new Gamma(shape, scale), j, j + 1);
                    Double countedEntalphy = countEntalphy(Helper.getLastItem(enthalpyInterpolation), currentTemp, currentTemp - interval);
                    addItemToDataSet(currentTemp, f.value(currentTemp), countedEntalphy + valueFromDistribution);
                    currentTemp = currentTemp + interval;
                }
            } else {
                Double countedEntalphy = countEntalphy(Helper.getLastItem(enthalpyInterpolation), currentTemp, currentTemp - step);
                addItemToDataSet(currentTemp, f.value(currentTemp), countedEntalphy);
                currentTemp = currentTemp + step;
            }
        }
    }


    public void loadDataFromTxt() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("Specific_Heat.txt"));
        String line = reader.readLine();
        while (line != null) {
            System.out.println(line);
            String[] parts = line.split(" ");
            temperature.add(Double.parseDouble(parts[0]) + 273.15); // from C to K
            specificHeat.add(Double.parseDouble(parts[1]));
            line = reader.readLine();
        }

        f = interp.interpolate(temperature.stream().mapToDouble(Double::doubleValue).toArray(),
                specificHeat.stream().mapToDouble(Double::doubleValue).toArray());

    }

    private void clearDatasetList() {
        enthalpyInterpolation.clear();
        temperatureForInterpolation.clear();
        specificHeatInterpolation.clear();
    }

    public XYDataset createDataset() {
        DefaultXYDataset ds = new DefaultXYDataset();
        double[][] data = {temperatureForInterpolation.stream().mapToDouble(Double::doubleValue).toArray(), enthalpyInterpolation.stream().mapToDouble(Double::doubleValue).toArray()};
        double last = Helper.getLastItem(enthalpyInterpolation);
        return ds;
    }

    public ArrayList<Double> getTemperature() {
        return temperature;
    }

    public ArrayList<Double> getSpecificHeat() {
        return specificHeat;
    }
}

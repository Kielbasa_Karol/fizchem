import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Kiełbasa Karol on 21.10.2018.
 */
public class Window {
    public JPanel panel1;
    private JPanel chartPanel;
    private JPanel dataPanel;
    private JPanel sketchPanel;
    private JTextField t1;
    private JTextField t2;
    private JTextField entalphyFromUser;
    private JButton calculateButton;
    private JComboBox comboBox1;
    private JButton loadButton;
    private JTextField textField1;


    private Logic logic;

    public Window(Logic logic) {
        this.logic = logic;
        init();
    }

    private void init() {


        comboBox1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {

            }
        });

        calculateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!t1.getText().isEmpty()
                        && !t2.getText().isEmpty()
                        && !entalphyFromUser.getText().isEmpty()) {

                    logic.calculateEntalphyGamma(Double.parseDouble(t1.getText()), Double.parseDouble(t2.getText()),
                            Double.parseDouble(entalphyFromUser.getText()),14.0,1.0);
                    XYDataset ds = logic.createDataset();
                    JFreeChart chart = ChartFactory.createXYLineChart("Test Chart",
                            "x", "y", ds, PlotOrientation.VERTICAL, true, true,
                            false);

                    ChartPanel scatterPlotPanel = new ChartPanel(chart);
                    sketchPanel.setLayout(new java.awt.BorderLayout());
                    sketchPanel.add(scatterPlotPanel, BorderLayout.CENTER);
                    sketchPanel.validate();
                }
            }
        });
    }
}

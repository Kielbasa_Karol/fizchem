import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.distribution.GammaDistribution;

/**
 * Created by Kiełbasa Karol on 19.11.2018.
 */
public class Gamma implements UnivariateFunction {

    private GammaDistribution gamma;

    public Gamma(Double shape, Double scale) {
        this.gamma = new GammaDistribution(shape,scale);
    }

    @Override
    public double value(double v) {
        return gamma.density(v);
    }
}
